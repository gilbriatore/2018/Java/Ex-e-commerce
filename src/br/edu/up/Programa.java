package br.edu.up;

import java.util.Scanner;

import br.edu.up.entidades.Carrinho;
import br.edu.up.entidades.Produto;
import br.edu.up.util.BancoDeDados;
import br.edu.up.util.Relatorio;

public class Programa {

	public static void main(String[] args) {

		BancoDeDados banco = new BancoDeDados();
		Carrinho carrinho = new Carrinho();
		Relatorio relatorio = new Relatorio();
		
		Scanner leitor = new Scanner(System.in);
		String operador = "o";

		do {
			
			System.out.println("Digite r para relat�rio, x para sair,");
			System.out.println("ou 1 a 10 para adicionar produto: ");	
			operador = leitor.nextLine();
			
			//Faz a impress�o do relat�rio
			if (operador.equals("r")) {
				relatorio.imprimir(carrinho.listaDeProdutos);
			}
			else if (!operador.equals("x")) {
				//Converter operador para n�mero;
				int numero = Integer.parseInt(operador);
				
				//Pega produto no banco de dados
				Produto produto = banco.listaDeProdutos.get(numero);
				
				//Adiciona produto no carrinho
				carrinho.listaDeProdutos.add(produto);
			}
			
			
			
			
		} while(!operador.equals("x"));
		
		System.out.println("Sistema encerrado!");
		leitor.close();
	}

}
