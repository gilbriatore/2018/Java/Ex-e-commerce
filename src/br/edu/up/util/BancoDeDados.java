package br.edu.up.util;

import java.util.ArrayList;
import java.util.List;
import br.edu.up.entidades.Produto;

public class BancoDeDados {
	
	public List<Produto> listaDeProdutos = new ArrayList<Produto>();
	
	/**
	 * M�todo construtor
	 * 1. tem o nome da classe;
	 * 2. n�o possui retorno;
	 */
	public BancoDeDados() {
		
		Produto p1 = new Produto("Livro", 50.00);
		Produto p2 = new Produto("CD", 20.00);
		Produto p3 = new Produto("Revista", 10.00);
		Produto p4 = new Produto("Fone de ouvido", 70.00);
		Produto p5 = new Produto("Bateria", 20.00);
		Produto p6 = new Produto("Filmadora", 1000.00);
		Produto p7 = new Produto("iPhone", 800.00);
		Produto p8 = new Produto("C�mera", 500.00);
		Produto p9 = new Produto("Vassoura", 12.00);
		Produto p10 = new Produto("Caixa de som", 350.00);
		
		listaDeProdutos.add(p1);
		listaDeProdutos.add(p2);
		listaDeProdutos.add(p3);
		listaDeProdutos.add(p4);
		listaDeProdutos.add(p5);
		listaDeProdutos.add(p6);
		listaDeProdutos.add(p7);
		listaDeProdutos.add(p8);
		listaDeProdutos.add(p9);
		listaDeProdutos.add(p10);
	
	}

}
