package br.edu.up.util;

import java.util.List;

import br.edu.up.entidades.Produto;

public class Relatorio {
	
	
	public void imprimir(List<Produto> lista) {
		
		double total = 0;
		
		for (Produto produto : lista) {
			
			System.out.print("Produto: " + produto.getNome());
			System.out.println(" Valor: " + produto.getValor()); 
			
			total += produto.getValor();
		}
		
		System.out.println("Total da compra: " + total);
	}

}
